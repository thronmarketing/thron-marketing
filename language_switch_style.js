var footerClass = undefined;

$(document).ready(function(){
footerClass = $('footer').attr('class');
if (footerClass == "english-footer"){
$('.lang_switcher_link').appendTo('footer > div.container > div > div.col-md-3.col-md-push-6.city').css('color', '#aaaeb1').css('padding-right', '10px').css('padding-left', '10px').css('text-decoration','none');
$('.lang_switcher_link')[0].after('|')
$('.lang_switcher_link')[0].text='EN';
$('.lang_switcher_link')[1].text='IT';
$('footer > div.container > div > div.col-md-3.col-md-push-9.city > a').hide();
}
else if (footerClass == "italian-footer"){
$('.lang_switcher_link').appendTo('footer > div.container > div > div.col-md-3.col-md-push-6.city').css('color', '#aaaeb1').css('padding-right', '10px').css('padding-left', '10px').css('text-decoration','none');
$('.lang_switcher_link')[0].after('|')
$('.lang_switcher_link')[0].text='IT';
$('.lang_switcher_link')[1].text='EN';
$('footer > div.container > div > div.col-md-3.col-md-push-9.city > a').hide();
}
});
