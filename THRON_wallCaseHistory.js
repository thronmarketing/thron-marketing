
var newTemplate= "<div class='THRON-Recommendation-wall-history-item-default THRON-Recommendation-wall-history-item-selector-default THRON-singol-item-classHistory' >"
									+'<a class="THRON-item-history-href" href="#: contentUrl #" target="_blank">'
									+	'<div  class="THRON-item-history-category THRON-element-dot">'
									+		'<div id="THRON-category-id" class="category " title="#: nameCategory #">#: nameCategory #</div>'
									+	'</div>'
									+	'<div class="THRON-item-history-image-level">'
									+    '<div class="THRON-item-history-image-container" style="background-image:url(\'#: urlCropContent #\');"></div>'
									+	   '<div class="THRON-item-history-image-container-logo" style="background-image:url(\'#: urlLogoimage #\')"></div>'
									+	'</div>'
									//+'  <div class="THRON-item-history-image-container" style="background-image:url(\'#: httpRequest##: clientId #-view.thron.com/api/xcontents/resources/delivery/getThumbnail/#: clientId #/#: contentThumbSize #/#: contentId #?scalemode=auto\');"></div>'
									+	'<div class="THRON-item-history-information">'
									+	'<div  class="THRON-item-history-title THRON-element-dot">'
									+		'<div id="THRON-title-id" class="THRON-item-history-value-title " title="#: contentName #">#: contentName #</div>'
									+	'</div>'
									+	'<div  class="THRON-item-history-value-description THRON-element-dot">#: contentDescription # </div>'
									+	'</div>'
									+'</a>'
									+"</div>";
	THRON.WidgetFramework.jQuery(document).ready(function() {

		RecommendationWallWidget = new THRON.widget.RecommendationWallHistory(RecommendationWallParamsCaseHistory);
		RecommendationWallWidget.modifyItemTemplate(newTemplate);
		RecommendationWallWidget.bind(THRON.widget.event.INIT_ERROR,function(selectedElement,returnData){});
		RecommendationWallWidget.render("recommendationWallCaseHis");
	});

	$( window ).resize(function() {
		var wallWidth = $("#recommendationWallCaseHis").width();
		if ((wallWidth <= 902 && wallWidth >= 895)|| (wallWidth <= 602 && wallWidth >= 595))
		RecommendationWallWidget.reset();
		
		});
