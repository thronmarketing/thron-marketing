
// configuration

/// config left/right box
var newTemplateLeftTech  = "<div class='THRON-recommendation-list-DiscoverMore' >"
+   '<a class="THRON-item-href" href="#: contentUrl #"  >'
+	  '<div class="THRON-item-image-level"><div class="THRON-item-image-container" style="background-image:url(\'//#: clientId #-cdn.thron.com/delivery/public/thumbnail/#: clientId #/#: contentId #/jqgzww/std/0x0/pcr.png?quality=90\');"></div></div>'
+		'<div class="THRON-item-information">'
+		'<div class="THRON-item-title">'
+			'<div class="title-value">'
+				'<span   class="title THRON-element-dot" title="#: contentName #">#: contentName #</span>'
+			'</div>'
+		'</div>'
+'		<div class="THRON-item-description THRON-element-dot">#: contentDescription #</div>'
+'    <div class="THRON-item-href title-link" href="#: contentUrl #" >Discover More > </div>'
+	 '</div>'
+  '</a>'
+"</div>";
var newTemplateRightTech  = "<div class='THRON-recommendation-list-DiscoverMore' >"
+   '<a class="THRON-item-href" href="#: contentUrl #"  >'
+	  '<div class="THRON-item-image-level"><div class="THRON-item-image-container" style="background-image:url(\'//#: clientId #-cdn.thron.com/delivery/public/thumbnail/#: clientId #/#: contentId #/jqgzww/std/0x0/pcr.png?quality=90\');"></div></div>'
+		'<div class="THRON-item-information">'
+		'<div class="THRON-item-title">'
+			'<div class="title-value">'
+				'<span   class="title THRON-element-dot" title="#: contentName #">#: contentName #</span>'
+			'</div>'
+		'</div>'
+'		<div class="THRON-item-description THRON-element-dot">#: contentDescription #</div>'
+'    <div class="THRON-item-href title-link" href="#: contentUrl #" >Discover More > </div>'
+	 '</div>'
+  '</a>'
+"</div>";
//define widget param


THRON.WidgetFramework.jQuery(document).ready(function() {



	RecommendationListWidget = new THRON.widget.RecommendationList(RecommendationContentListParamsLeftTech);
	RecommendationListWidget.modifyItemTemplate(newTemplateLeftTech);
	RecommendationListWidget.render("discoverMoreLeftTech");

	RecommendationListWidget = new THRON.widget.RecommendationList(RecommendationContentListParamsRightTech);
	RecommendationListWidget.modifyItemTemplate(newTemplateRightTech);
	RecommendationListWidget.render("discoverMorerightTech");
});
