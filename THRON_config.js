
//video embedd code params
var clientIdVideo='hub';
var pKeyIdVideo='z3hoht';


//configurazione hero homepage
var  arrayHeroTagCheck=[ {"classificationId": "zsiqzc",
"id": "c300500b08d34dfe8c40eede8c39bed0"
}];
//find content HERO
var idHeroContentCheck="9a7a4d9d-71a6-4d18-beb7-1096ae647d67";

//init WIDGET Hero
var RecommendationWallParamsHero = {
  clientId: "hub",
  appId: "RC-5HJ2YB",
  predictiveContentRecommendation: "RC-V9OKFX",
  dictionaryLocale : "EN",
  autoTracking:false,
  thumbSize:"420x0",
  thumbSizeLogo:"0x80",
  maxElement:1,
  cellBlockSize:320,
  cellGutterX:22,
  cellGutterY:22,
  locales: 'IT,EN'
};
//homepage hero configuration widget
var RecommendationWallParamsVerify = {
  clientId: "hub",
  appId: "RC-5HJ2YB",
  predictiveContentRecommendation: "RC-V9OKFX",
  locales: "IT,EN",
  dictionaryLocale : "IT",
  //actionFunction:getPrimaryContent,  definita nel file di script della homepage
  tagsFilter: arrayHeroTagCheck
};
//video hero configuration
var RecommendationWallParamsVideo = {
  clientId: "hub",
  appId: "RC-F7NKK4",
  predictiveContentRecommendation: "RC-V9OKFX",
  locales: "IT,EN",
  dictionaryLocale : "EN"
  //actionFunction:startVideo definita nel file di script della homepage
};
//pcr cuastomers e blog item
var RecommendationContentListParams3Content = {
  clientId: "hub",
  appId: "RC-8SHPOF",
  predictiveContentRecommendation: "RC-V9OKFX",
  thumbSize: "348x176",// thumbnail dimension
  minBoxWidth: 338,    // min  block wall dimension
  maxElement:3,        // limit elmento display
  autoplay:false,       // force no autoplay
  locales: "IT,EN",
  dictionaryLocale : "EN"
};

//pcr homepage
var RecommendationContentListParams3ContentHome = {
    clientId: "hub",
    appId: "RC-L3RSB5",
    predictiveContentRecommendation: "RC-V9OKFX",
    thumbSize: "348x176",// thumbnail dimension
    minBoxWidth: 338,    // min  block wall dimension
    maxElement:3,        // limit elmento display
    autoplay:false,       // force no autoplay
    locales: "IT,EN",
    dictionaryLocale : "EN"
  };
//pcr industries configuration
var RecommendationContentListParams3ContentIndustries = {
  clientId: "hub",
  appId: "RC-FLVXZC",
  predictiveContentRecommendation: "RC-V9OKFX",
  thumbSize: "348x176",// thumbnail dimension
  minBoxWidth: 338,    // min  block wall dimension
  maxElement:3,        // limit elmento display
  autoplay:false,       // force no autoplay
  locales: "IT,EN",
  dictionaryLocale : "EN"
};

//case history wall config

var RecommendationWallParamsCaseHistory = {
  clientId: "hub",
  appId: "RC-6LEFAM",
  fallBack: "none",
  predictiveContentRecommendation: "RC-V9OKFX",
  locales: "IT,EN",
  dictionaryLocale : "EN",
  thumbSize:"350x350",
  thumbSizeLogo:"350x0",
  autoTracking:false,
  cellBlockSize:300,
  cellGutterX:22,
  cellGutterY:22,
};

//config wall top
	var RecommendationWallParamsSoftwareTop = {
			clientId: "hub",
			appId: "RC-6LEFAM",
			predictiveContentRecommendation: "RC-V9OKFX",
			locales: "IT,EN",
			dictionaryLocale : "EN",
			autoTracking:false,
			thumbSize:"0x0",
			thumbSizeLogo:"0x80",
			maxElement:1,
			cellBlockSize:250,
			cellGutterX:22,
			cellGutterY:22,
		};

//config wall left
    var RecommendationContentListParamsLeft = {
        //configuration widget param
    clientId: "hub",
        appId: "RC-XGD0ET",
        predictiveContentRecommendation: "RC-V9OKFX",
        thumbSize: "495x300", // thumbnail dimension
        minBoxWidth: 495,		  // min  block wall dimension
        maxElement:1, 				// limit elmento display
        autoplay:false, 			// force no autoplay
        locales: "IT,EN",
        dictionaryLocale : "EN"
    };
    //config wall right
        var RecommendationContentListParamsRight = {
            //configuration widget param
        clientId: "hub",
            appId: "RC-BDWN2J",
            predictiveContentRecommendation: "RC-V9OKFX",
            thumbSize: "495x300", // thumbnail dimension
            minBoxWidth: 495,		  // min  block wall dimension
            maxElement:1, 				// limit elmento display
            autoplay:false, 			// force no autoplay
            locales: "IT,EN",
            dictionaryLocale : "EN"
        };

        //config wall left
            var RecommendationContentListParamsLeftTech = {
                //configuration widget param
            clientId: "hub",
                appId: "RC-04VTKG",
                predictiveContentRecommendation: "RC-V9OKFX",
                thumbSize: "495x300", // thumbnail dimension
                minBoxWidth: 495,		  // min  block wall dimension
                maxElement:1, 				// limit elmento display
                autoplay:false, 			// force no autoplay
                locales: "IT,EN",
                dictionaryLocale : "EN"
            };
            //config wall right
                var RecommendationContentListParamsRightTech = {
                    //configuration widget param
                clientId: "hub",
                    appId: "RC-HZELW8",
                    predictiveContentRecommendation: "RC-V9OKFX",
                    thumbSize: "495x300", // thumbnail dimension
                    minBoxWidth: 495,		  // min  block wall dimension
                    maxElement:1, 				// limit elmento display
                    autoplay:false, 			// force no autoplay
                    locales: "IT,EN",
                    dictionaryLocale : "EN"
                };
