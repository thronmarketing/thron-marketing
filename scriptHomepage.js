//#########################################################################################
//config widget
// obj widget video
var RecommendationWallWidget=null;
// obj widget video
var RecommendationWallWidgetVideo = null;
// config find content recommendation
var RecommendationWallWidgetFirst = null;

//template Recommendation-wall-history
var newTemplateHero= "<div class='THRON-Recommendation-wall-history-item-default THRON-Recommendation-wall-history-item-selector-default THRON-singol-item-classWall' >"
+'<a class="THRON-item-history-href" target="_blank" href="#: contentUrl #" >'
/*	+	'<div  class="THRON-item-history-category THRON-element-dot">'
+		'<div id="THRON-category-id" class="category " title="#: nameCategory #">#: nameCategory #GLAXOSMITHKLINE</div>'
+	'</div>'*/
+	'<div class="THRON-item-history-item">'
+	 '<div class="THRON-item-history-image-level">'
+    '<div class="THRON-item-history-image-container" style="background-image:url(\'#: urlCropContent #\');"></div>'
+	 '<div class="THRON-item-history-image-container-logo" style="background-image:url(\'#: urlLogoimage #\')"></div>'
+	 '</div>'
+	'</div>'
+	'<div class="THRON-item-history-information">'
+'<div id="THRON-title-id" class="THRON-item-history-value-title THRON-element-dot" title="#: contentName #">#: contentName #</div>'
+	'<div  class="THRON-item-history-value-subtitle THRON-element-dot ">#: nameSubtitle # </div>'
+	'<div  class="THRON-item-history-value-description THRON-element-dot">#: contentDescription # </div>'
+	'</div>'
+	'<div  class="THRON-item-history-value-link">Download the Report </div>'
+'</a>'
+"</div>";



//#########################################################################################
// start video embed
function startVideo(objparam)
{

	var indexVideo = 0;
	var name_subtitle_customer = "";
	var contentID = -1;
	var idLogo = "";               // xcontentid logo
	var custom_poster = -1; // id other poster  ...default original id
	// verifica primo video raccomandato
	for(var i=0;i<objparam.data.length;i++){
		if(objparam.data[i].content.contentType=="VIDEO"){
			indexVideo=i;
			i=objparam.data.length;

		}

	}


  //start value content
	contentID=objparam.data[indexVideo].content.id;
	custom_poster=contentID;
	//ricerca dati
	for(var i=0;i<objparam.data[indexVideo].content.imetadata.length;i++){
		if(objparam.data[indexVideo].content.imetadata[i].key=="id_logo_customer"){
			idLogo=objparam.data[indexVideo].content.imetadata[i].value;
		};
		if(objparam.data[indexVideo].content.imetadata[i].key=="custom_poster"){
			custom_poster=objparam.data[indexVideo].content.imetadata[i].value;
		};
		if(objparam.data[indexVideo].content.imetadata[i].key=="name_subtitle_customer"){
			name_subtitle_customer=objparam.data[indexVideo].content.imetadata[i].value;
		};
	}

	var options = {
		clientId: clientIdVideo,
		xcontentId: contentID,
		idLogo:idLogo,
		custom_poster:custom_poster,
		sessId: pKeyIdVideo,
		name_subtitle_customer: name_subtitle_customer

	};
	var player = THRONContentExperience("aptov", options);
};
// function check content Present
/*function getPrimaryContent(dataRecommendation){

	var findIdContent=false
	for(var i=0;i<dataRecommendation.data.length;i++){
		if(dataRecommendation.data[i].content.id==idHeroContentCheck){
			findIdContent=true;
		}
	}
	console.log("getPrimaryContent",findIdContent,dataRecommendation)

	//enable banner
	if(findIdContent==true || dataRecommendation.data.length == 0)	{

		//$(".TH-primary-hero").hide().fadeIn(4000).addClass('active_hero');
		$('.TH-primary-hero').fadeIn(4000).addClass('active_hero').css('max-height','830px')
		$(".TH-widget-hero").show().addClass('active_hero_widget');
	}
	else{
		//   $("#hs_cos_wrapper_widget_1500909211745").show();
		$(".TH-widget-hero").show().addClass('active_hero_widget');
		// TODO creare effetto attivazione widget
		$(".TH-widget-hero").parent().insertBefore($(".thron-azienda").parent());
		$(".TH-widget-hero ").addClass("TH-SpecialHero")
	}


	RecommendationWallWidget = new THRON.widget.RecommendationWallHistory(RecommendationWallParamsHero);
	RecommendationWallWidget.modifyItemTemplate(newTemplateHero);
	RecommendationWallWidget.render("recommendationHeroWall");

}*/
//###################################################################
/* MAIN enable hero banner*/
//###################################################################
$( document ).ready(function(){
	//console.log('start')
	// controllo se  devo caricare il banner principale o la hero
	//RecommendationWallParamsVerify['actionFunction']=getPrimaryContent;
	//RecommendationWallWidget = new THRONwidgetRecommendationHero(RecommendationWallParamsVerify);
	//RecommendationWallWidget.start();
	//console.log("config VErify wall",RecommendationWallParamsVerify)
	// esecuzione caricamento video
 var actionFade=false;
		RecommendationWallWidgetFirst = new THRON.widget.RecommendationWallHistory(RecommendationWallParamsHero);
		RecommendationWallWidgetFirst.modifyItemTemplate(newTemplateHero);
		RecommendationWallWidgetFirst.render("recommendationHeroWall_firstVist");

		RecommendationWallWidgetFirst.bind(THRON.widget.event.LOAD_COMPLETE,function(){
			if(actionFade == false)
			  $('#recommendationHeroWall_firstVist .THRON-Recommendation-wall-history-default').fadeTo(2000, 1);
			actionFade=true;

		});

	RecommendationWallWidget = new THRON.widget.RecommendationWallHistory(RecommendationWallParamsHero);
	RecommendationWallWidget.modifyItemTemplate(newTemplateHero);
	RecommendationWallWidget.render("recommendationHeroWall");

	RecommendationWallParamsVideo['actionFunction']=startVideo;
	var RecommendationWallWidgetVideo = new THRONwidgetRecommendationHero(RecommendationWallParamsVideo);
	RecommendationWallWidgetVideo.start();
	//console.log("config VErify video",RecommendationWallParamsVideo)
	//console.log('start2',RecommendationContentListParams3Content,RecommendationWallParamsVideo)
	// set widet  3 content


});
