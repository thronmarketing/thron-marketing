

/*
//############################################################################################
MAIN
//############################################################################################
*/

// Inheritance generic widget


THRONwidgetRecommendationHero = function(params) {
	//client id
	if(params.clientId !== undefined){
		this.clientId = params.clientId;
	} else {
		throw "Undefined ClientId";
	}
	this.LOAD_COMPLETE="LOAD_COMPLETE";
	// locale widget
	this.httpRequest = (params.httpRequest !== undefined) ? params.httpRequest : "//";
	// locale widget
	this.localeWidget = (params.locales !== undefined) ? params.locales : "IT,EN";
	//  dictionary name default
	this.dictionaryLocale = (params.dictionaryLocale !== undefined) ? params.dictionaryLocale : "EN";

	//WIDGET NEW PARAM CONFIG
	this.appIDMASTER= (params.predictiveContentRecommendation !== undefined) ? params.predictiveContentRecommendation : "";	//that.appIDMASTER
	this.appId= (params.appId !== undefined) ? params.appId : "";//SLAVE
	//action  function when there is the info from recomandation
	this.actionFunction= (params.actionFunction !== undefined) ? params.actionFunction : console.log; //"none","showcontent";
	//max element load
	this.maxElement= (params.maxElement !== undefined) ? params.maxElement : 20;
	this.tagsFilter= (params.tagsFilter !== undefined) ? params.tagsFilter : [];
	this.xTokenId=undefined;
	this.rendered = false;
	this.update();

};//class

THRONwidgetRecommendationHero.prototype = new THRON.widget.Widget();
//############################################################################################
//Default message Action NO ITEM
//############################################################################################
THRONwidgetRecommendationHero.prototype.noItemHero=  function(){
	this.actionFunction({totalResults:0})
}
//############################################################################################
// get information widget
//############################################################################################
THRONwidgetRecommendationHero.prototype.getInformationHero =  function(data){

	var that=this;

//that.dispatch(this.LOAD_COMPLETE,{totalResults:data.recommendedContents.length});
that.actionFunction({totalResults:data.recommendedContents.length,data:data.recommendedContents})

}

//############################################################################################
// get  WS information
//############################################################################################
THRONwidgetRecommendationHero.prototype.iniActionRecommendationHero =  function(){
	var that=this;
	//default URL
	var urlRacc= that.httpRequest+that.clientId+"-view.thron.com/pcr/xrecommendation/resources/content/listByCriteria/"+that.clientId+"/"+ that.appIDMASTER+"/"+ that.appId;/*+"&lastViewedContents="+that.contentsViewed*/
+"?contactId="+that.contactId+"&lang="+that.localeWidget
	var criteria ={"contactId": that.contactId,
								 "lang": that.localeWidget,
								 "mode": "RECOMMENDED",
								 "tags": that.tagsFilter
								}

	// get Recommendation data
	window.THRON.WidgetFramework.jQuery.ajax({
																						type: "POST",
																						url:urlRacc,
																						data: JSON.stringify({ criteria: criteria }),
																				    contentType: "application/json; charset=utf-8",
																				    dataType: "json",
																					}).done(function(data){
																									//have value
																									that.getInformationHero(data);
																										//fail
																									}).fail(function (jqXHR, textStatus) 	{
																										that.noItemHero()
																									})

}

//############################################################################################
// restart widgt
//############################################################################################
THRONwidgetRecommendationHero.prototype.start = function() {

	this.rendered = false;
	this.update();
}
//############################################################################################
//Load tracking and load item Recommendation
//############################################################################################
THRONwidgetRecommendationHero.prototype.loadItemHero = function() {
	var that = this;
	that.contactId="";

	//get contact ID
	window.THRON.WidgetFramework.jQuery.ajax({
		method: "GET",
		dataType: "script",
		url: that.httpRequest + that.clientId + "-cdn.thron.com/shared/plugins/tracking/current/tracking-library-min.js"
	}).done(function( response,trackLib )	{
		//contact
		_ta.getContactInformation(that.clientId,function (data)
		{
			//contact id
			that.contactId=data.detail.contactId
			that.iniActionRecommendationHero(false) ;
		});
	});
}
//############################################################################################
//update Recommendation infor
//############################################################################################
THRONwidgetRecommendationHero.prototype.update = function() {


	var that = this;

	//main action request info
	if(!that.rendered)
	{
		//laod item
		that.loadItemHero();
		that.rendered = true;
	}
};
