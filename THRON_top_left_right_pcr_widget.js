
// configuration


var newTemplateWall1= "<div class='THRON-Recommendation-wall-history-item-default THRON-Recommendation-wall-history-item-selector-default THRON-singol-item-classWall' >"
+'<a class="THRON-item-history-href" href="#: contentUrl #" >'
+	'<div class="THRON-item-history-image-level">'
+    '<div class="THRON-item-history-image-container" style="background-image:url(\'#: urlCropContent #\');"></div>'
+	'</div>'
+	'<div class="container">'
+	'<div class="THRON-item-history-information">'
+	'<div class="THRON-item-history-image-level2">'
+	   '<div class="THRON-item-history-image-container-logo" style="background-image:url(\'#: urlLogoimage #\')"></div>'
+	'</div>'
+	'<div  class="THRON-item-history-value-description THRON-element-dot">#: contentDescription # </div>'
+	'<div  class="THRON-item-history-value-link">View the Case History </div>'
+	'</div>'
+	'</div>'
+	'<div class="THRON-item-history-href-gradient"></div>'
+'</a>'
+"</div>";

/// config left/right box
var newTemplateLeft  = "<div class='THRON-recommendation-list-DiscoverMore' >"
+   '<a class="THRON-item-href" href="#: contentUrl #"  >'
+	  '<div class="THRON-item-image-level"><div class="THRON-item-image-container" style="background-image:url(\'//#: clientId #-view.thron.com/api/xcontents/resources/delivery/getThumbnail/#: clientId #/#: contentThumbSize #/#: contentId #?scalemode=auto&quality=85\');"></div></div>'
+		'<div class="THRON-item-information">'
+		'<div class="THRON-item-title">'
+			'<div class="title-value">'
+				'<span   class="title THRON-element-dot" title="#: contentName #">#: contentName #</span>'
+			'</div>'
+		'</div>'
+'		<div class="THRON-item-description THRON-element-dot">#: contentDescription #</div>'
+'    <div class="THRON-item-href title-link" href="#: contentUrl #" >Discover More > </div>'
+	 '</div>'
+  '</a>'
+"</div>";
var newTemplateRight  = "<div class='THRON-recommendation-list-DiscoverMore' >"
+   '<a class="THRON-item-href" href="#: contentUrl #"  >'
+	  '<div class="THRON-item-image-level"><div class="THRON-item-image-container" style="background-image:url(\'//#: clientId #-cdn.thron.com/delivery/public/thumbnail/#: clientId #/#: contentId #/jqgzww/std/0x0/pcr.png?quality=90\');"></div></div>'
+		'<div class="THRON-item-information">'
+		'<div class="THRON-item-title">'
+			'<div class="title-value">'
+				'<span   class="title THRON-element-dot" title="#: contentName #">#: contentName #</span>'
+			'</div>'
+		'</div>'
+'		<div class="THRON-item-description THRON-element-dot">#: contentDescription #</div>'
+'    <div class="THRON-item-href title-link" href="#: contentUrl #" >Discover More > </div>'
+	 '</div>'
+  '</a>'
+"</div>";
//define widget param


THRON.WidgetFramework.jQuery(document).ready(function() {

	RecommendationWallWidget = new THRON.widget.RecommendationWallHistory(RecommendationWallParamsSoftwareTop);
	RecommendationWallWidget.modifyItemTemplate(newTemplateWall1);
	RecommendationWallWidget.render("recommendationWallSoftware");


	RecommendationListWidget = new THRON.widget.RecommendationList(RecommendationContentListParamsLeft);
	RecommendationListWidget.modifyItemTemplate(newTemplateLeft);
	RecommendationListWidget.render("discoverMoreLeft");

	RecommendationListWidget = new THRON.widget.RecommendationList(RecommendationContentListParamsRight);
	RecommendationListWidget.modifyItemTemplate(newTemplateRight);
	RecommendationListWidget.render("discoverMoreright");
});
