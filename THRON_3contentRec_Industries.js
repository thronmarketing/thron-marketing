//define  item html structure 3 Video
var newTemplate3Content  = "<div class='THRON-recommendation-list-item-sito' >"
+   '<a class="THRON-item-href" href="#: contentUrl #"  >'
+	   '<div class="THRON-item-image-level "><div class="THRON-item-image-container" style="background-image:url(\'//#: clientId #-cdn.thron.com/delivery/public/thumbnail/#: clientId #/#: contentId #/jqgzww/std/0x0/pcr.png?quality=90\');"></div></div>'
+	   	'<div class="THRON-item-information">'
+		  '<div class="THRON-item-title">'
+			'<div class="title-value " >'
+				'<div   class="title THRON-element-dot" title="#: contentName #">#: contentName #</div>'
+			'</div>'
+		 '</div>'
+		 '<div class="THRON-item-description THRON-element-dot">#: contentDescription #</div>'
+	  '</div>'
+  '</a>'
+"</div>";

$( document ).ready(function(){
var	RecommendationListWidget3Video = new THRON.widget.RecommendationList(RecommendationContentListParams3ContentIndustries);
RecommendationListWidget3Video.modifyItemTemplate(newTemplate3Content);
RecommendationListWidget3Video.render("THRON-industries-widget");
});
